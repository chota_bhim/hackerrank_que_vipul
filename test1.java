import java.util.*;
class test1
{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s;
		s = sc.nextLine();
		int res = 0;
		StringBuffer sb;
		 for (int len = 1; len <= s.length(); len++) {  
            for (int i = 0; i <= s.length() - len; i++) { 
                int j = i + len - 1; 
                String temp = "";
                for (int k = i; k <= j; k++) { 
                    temp += s.charAt(k); 
                } 
  				sb = new StringBuffer(temp);
  				if (temp.equals(sb.reverse().toString()))
  					res++;
            } 
        }
        System.out.println(res); 
	}
}